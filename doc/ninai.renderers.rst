
ninai.renderers package
***********************


Submodules
==========


ninai.renderers.bn module
=========================

**ninai.renderers.bn.process_scope_outputs(base_clause, outputs)**

**ninai.renderers.bn.render_action_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_attribution_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_distal_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_existence_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_identification_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_instance_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_listener_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_locative_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_medial_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_negation_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_proximal_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_speaker_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.bn.render_superlative_bn(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**


ninai.renderers.de module
=========================

**ninai.renderers.de.process_scope_outputs(attribute_clause,
outputs)**

**ninai.renderers.de.render_action_de(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.de.render_attribution_de(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.de.render_identification_de(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.de.render_instance_de(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.de.render_locative_de(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.de.sense_refine_de(candidate_paths, new_config,
**scope)**


ninai.renderers.mul module
==========================

**ninai.renderers.mul.render_crastinal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_definite(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_emphasis(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_familiar(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_formal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_futuretemporal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_hesternal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_hodiernal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_informal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_pasttemporal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_paucal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_plural(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_presenttemporal(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.mul.render_singular(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**


ninai.renderers.sv module
=========================

**ninai.renderers.sv.process_scope_outputs(attribute_clause,
outputs)**

**ninai.renderers.sv.propagate_scope_outputs(outputs)**

**ninai.renderers.sv.render_action_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_attribution_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_existence_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_identification_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_instance_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_listener_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_locative_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_negation_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_speaker_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**

**ninai.renderers.sv.render_superlative_sv(inputs: types.Inputs) ->
`ninai.base.constructor.ConstructorOutput <ninai.base.rst#ninai.base.constructor.ConstructorOutput>`_**


Module contents
===============


ninai.concepts package
**********************


Module contents
===============

**class ninai.concepts.GeneralDemonym(*args)**

Bases: `ninai.base.concept.Concept <ninai.base.rst#ninai.base.concept.Concept>`_

``_abc_impl = <_abc._abc_data object>``

  **render(language: tfsl.languages.Language, context: Optional[list]
= None, framing: Optional[dict] = None)**

Provides a Clause object representing ``object_in`` in language
``language_in``.

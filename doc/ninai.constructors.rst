
ninai.constructors package
**************************


Submodules
==========


ninai.constructors.locatives module
===================================

**class ninai.constructors.locatives.ActionLocation(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for the location of a constructor in motion. These may
be used, for example, as scope arguments of other constructors.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('object',)``

**class ninai.constructors.locatives.Adessive(*args)**

Bases: `ninai.constructors.locatives.StativeLocation <#ninai.constructors.locatives.StativeLocation>`_

Used to indicate that the containing constructor is near a
particular object:

::

>>> Attribution(Concept(Q(319)), Adessive(Concept(Q(544)))) # Jupiter is near the solar system.

(See also general notes regarding StativeLocation.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('object',)``

**class ninai.constructors.locatives.Locative(*args)**

Bases: `ninai.constructors.locatives.StativeLocation <#ninai.constructors.locatives.StativeLocation>`_

Used to indicate the generic location of a particular object (akin
to ‘in’, ‘at’, ‘on’):

::

>>> Attribution(Concept(Q(319)), Location(Concept(Q(544)))) # Jupiter is in the solar system.

(See also general notes regarding StativeLocation.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('object',)``

**class ninai.constructors.locatives.StativeLocation(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for the location of a constructor not in motion. These
may be used, for example, as the second argument of Attribution.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('object',)``

**class ninai.constructors.locatives.Subessive(*args)**

Bases: `ninai.constructors.locatives.StativeLocation <#ninai.constructors.locatives.StativeLocation>`_

Used to indicate that the containing constructor is below a
particular object:

::

>>> Attribution(Concept(Q(319)), Subessive(Concept(Q(544)))) # Jupiter is below the solar system.

(See also general notes regarding StativeLocation.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('object',)``


ninai.constructors.modifiers module
===================================

**class ninai.constructors.modifiers.Comparative(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to specify that an attribute is more pronounced compared to
some other object.

>>> Attribution(Concept(Q(319)), Comparative(Concept(Q(59863338)), Concept(Q(193)))) # Jupiter is larger than Saturn.

The second argument could be omitted to provide a comparative form
explicitly.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('attribute', 'object')``

**class ninai.constructors.modifiers.Definite(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to mark an instance of a concept as definite:

::

>>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
>>> Identification(Concept(Q(319)), Instance(Concept(Q(634)), Definite())) # Jupiter is the planet.

There may be instances, even in languages where definiteness is not
marked, where omitting this constructor may not make a difference,
but omitting it might yield undesirable or inconsistent results.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Demonstrative(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for demonstratives.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Distal(*args)**

Bases: `ninai.constructors.modifiers.Demonstrative <#ninai.constructors.modifiers.Demonstrative>`_

Used to specify that the contained constructor is away from the
speaker and the listener (much like ‘ano’ in Japanese):

::

>>> Attribution(Distal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

(See also general notes regarding Demonstrative.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Emphasis(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Metalinguistic emphasis, which might well occur anywhere and be
rendered in many ways:

::

>>> Identification(Distal(Concept(Q(575))), Concept(Q(3635662))) # সে রাত পূর্ণিমা।
>>> Identification(Distal(Concept(Q(575)), Emphasis()), Concept(Q(3635662))) # সেই রাত পূর্ণিমা।

(More examples to come when more requirements of emphasis handling
arise in other languages.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Familiar(*args)**

Bases: `ninai.constructors.modifiers.Honorific <#ninai.constructors.modifiers.Honorific>`_

Used to adjust the containing constructor to reflect a familiar
inflection (the level indicated by ‘tum’ in Hindustani):

::

>>> Identification(Listener(Familiar()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

(See also general notes regarding Honorific.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.FarDistal(*args)**

Bases: `ninai.constructors.modifiers.Demonstrative <#ninai.constructors.modifiers.Demonstrative>`_

Used to specify that the contained constructor is away from the
speaker and the listener and somewhat far (much like ‘dot’ in
Northern Sami):

::

>>> Attribution(FarDistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

(See also general notes regarding Demonstrative.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Formal(*args)**

Bases: `ninai.constructors.modifiers.Honorific <#ninai.constructors.modifiers.Honorific>`_

Used to adjust the containing constructor to reflect a formal
inflection (the level indicated by ‘aap’ in Hindustani):

::

>>> Identification(Listener(Formal()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

(See also general notes regarding Honorific.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Honorific(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for honorific specifiers.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Informal(*args)**

Bases: `ninai.constructors.modifiers.Honorific <#ninai.constructors.modifiers.Honorific>`_

Used to adjust the containing constructor to reflect an informal
inflection (the level indicated by ‘tu’ in Hindustani):

::

>>> Identification(Listener(Informal()), Possession(Speaker(), Concept(Q(17297777)))) # Thou art my friend.

(See also general notes regarding Honorific.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Instance(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to define a particular instance of a concept as an argument of
a constructor, rather than the concept by itself:

::

>>> Identification(Concept(Q(319)), Concept(Q(634))) # Jupiter is planet (the concept).
>>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

There may be instances, even in languages where definiteness is not
marked, where omitting this constructor may not make a difference,
but omitting it might yield undesirable or inconsistent results.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('subject',)``

**class ninai.constructors.modifiers.MedialDemonstrative(*args)**

Bases: `ninai.constructors.modifiers.Demonstrative <#ninai.constructors.modifiers.Demonstrative>`_

Used to specify that the contained constructor is closer to the
listener (much like ‘sono’ in Japanese):

::

>>> Attribution(MedialDemonstrative(Concept(Q(10978))), Concept(Q(24245823))) # That grape is small.

(See also general notes regarding Demonstrative.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Mesiodistal(*args)**

Bases: `ninai.constructors.modifiers.Demonstrative <#ninai.constructors.modifiers.Demonstrative>`_

Used to specify that the contained constructor is away from the
speaker and the listener but still near (much like ‘duot’ in
Northern Sami):

::

>>> Attribution(Mesiodistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

(See also general notes regarding Demonstrative.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

``ninai.constructors.modifiers.Mesioproximal``

alias of `ninai.constructors.modifiers.MedialDemonstrative <#ninai.constructors.modifiers.MedialDemonstrative>`_

**class ninai.constructors.modifiers.Negation(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to negate different types of constructors:

::

>>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
>>> Negation(Identification(Concept(Q(319)), Instance(Concept(Q(634))))) # Jupiter is not a planet.

Essentially an equivalent to the logical ‘not’ operator.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Paucal(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Paucalizes(?) the constructor it modifies:

::

>>> Possession(Speaker(), Paucal(Concept(Q(133105)))) # I have a few legs.

(This and other constructors without renderers are subject to
abrupt change.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Plural(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Pluralizes the constructor it modifies:

::

>>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

(This and other constructors without renderers are subject to
abrupt change.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Proximal(*args)**

Bases: `ninai.constructors.modifiers.Demonstrative <#ninai.constructors.modifiers.Demonstrative>`_

Used to specify that the contained constructor is closer to the
speaker (much like ‘kono’ in Japanese):

::

>>> Attribution(Proximal(Concept(Q(10978))), Concept(Q(24245823))) # This grape is small.

(See also general notes regarding Demonstrative.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Reason(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to indicate that the contained constructor is a cause of the
statement that contains this modifier:

::

>>> Existence(Listener(), Reason(Possession(Listener(), Concept(Q(9165))))) # You exist because you have a soul.

The handling of extra arguments beyond the first is yet to be
determined.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('constructor',)``

**class ninai.constructors.modifiers.Singular(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Pluralizes the constructor it modifies:

::

>>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

(This and other constructors without renderers are subject to
abrupt change.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.modifiers.Superlative(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Used to specify that an attribute is the most pronounced:

::

>>> Attribution(Concept(Q(319)), Superlative(Concept(Q(59863338)))) # Jupiter is the largest planet.

As with other constructors, extra arguments may be added to clarify
the superlative nature of the attribute.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('attribute',)``


ninai.constructors.nominals module
==================================

**class ninai.constructors.nominals.Listener(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Refers to a listener:

::

>>> Attribution(Listener(), Concept(Q(59863338))) # You are large.

(This will likely be adjusted when a renderer for it is created.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.nominals.Speaker(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Refers to the speaker:

>>> Attribution(Speaker(), Concept(Q(59863338))) # I am large.

(This will likely be adjusted when a renderer for it is created.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``


ninai.constructors.nonverbal_predicates module
==============================================

**class ninai.constructors.nonverbal_predicates.Attribution(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Assigns an attribute to an object:

::

>>> Attribution(Concept(Q(319)), Concept(Q(59863338))

Independently the above may represent “Jupiter is large”, a
complete statement. As an argument elsewhere it may represent
“large Jupiter”, a modified concept.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('subject', 'attribute')``

**class ninai.constructors.nonverbal_predicates.Benefaction(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Indicates that something is for the benefit of something else:

::

>>> Benefaction(Concept(Q(319)), Instance(Concept(Q(58968)), Plural()))

Independently it may represent “Jupiter is for intellectuals”, a
complete statement. As an argument elsewhere it may represent
“Jupiter for intellectuals”, a modified concept.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('subject', 'beneficiary')``

**class ninai.constructors.nonverbal_predicates.Existence(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Indicates that something exists:

::

>>> Existence(Instance(Concept(Q(634)))) # A planet exists; there is a planet.

At the moment considered only as a top-level constructor.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('existent',)``

**class
ninai.constructors.nonverbal_predicates.Identification(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Equates subject and object:

::

>>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

At the moment considered only as a top-level constructor.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('subject', 'object')``

**class ninai.constructors.nonverbal_predicates.Possession(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Indicates that something owns something else:

::

>>> Possession(Concept(Q(319)), Instance(Concept(Q(179792))))

Independently it may represent “Jupiter has a ring”, a complete
statement. As an argument elsewhere it may represent “Jupiter’s
ring”, a modified concept.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('possessor', 'possessed')``


ninai.constructors.temporals module
===================================

**class ninai.constructors.temporals.Crastinal(*args)**

Bases: `ninai.constructors.temporals.FutureTemporal <#ninai.constructors.temporals.FutureTemporal>`_

Used for events occurring on the day following the current
reference time:

::

>>> Possession(Speaker(), Instance(Concept(Q(10817602))), Crastinal()) # I will have a pineapple tomorrow.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.FutureTemporal(*args)**

Bases: `ninai.constructors.temporals.GeneralTemporal <#ninai.constructors.temporals.GeneralTemporal>`_

Used for events occurring at any time after the current reference
time:

::

>>> Possession(Speaker(), Instance(Concept(Q(10817602))), FutureTemporal()) # I will have a pineapple.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.GeneralTemporal(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for modifiers specifying the generic times of
constructors (akin to providing a tense).

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.Hesternal(*args)**

Bases: `ninai.constructors.temporals.PastTemporal <#ninai.constructors.temporals.PastTemporal>`_

Used for events occurring on the day prior to the current reference
time:

::

>>> Possession(Speaker(), Instance(Concept(Q(89))), Hesternal()) # I had an apple yesterday.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.Hodiernal(*args)**

Bases: `ninai.constructors.temporals.PresentTemporal <#ninai.constructors.temporals.PresentTemporal>`_

Used for events occurring on the same day as the current reference
time:

::

>>> Possession(Speaker(), Instance(Concept(Q(165447))), Hodiernal()) # I have a pen today.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.PastTemporal(*args)**

Bases: `ninai.constructors.temporals.GeneralTemporal <#ninai.constructors.temporals.GeneralTemporal>`_

Used for events occurring at any time before the current reference
time:

::

>>> Possession(Speaker(), Instance(Concept(Q(89))), PastTemporal()) # I had an apple.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.PresentTemporal(*args)**

Bases: `ninai.constructors.temporals.GeneralTemporal <#ninai.constructors.temporals.GeneralTemporal>`_

Used for events occurring at or around the current reference time:

::

>>> Possession(Speaker(), Instance(Concept(Q(165447))), PresentTemporal()) # I have a pen.

(See also general notes regarding GeneralTemporal.)

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

**class ninai.constructors.temporals.SpecificTemporal(*args)**

Bases: `ninai.base.constructor.Constructor <ninai.base.rst#ninai.base.constructor.Constructor>`_

Base class for modifiers specifying the times of constructors based
on a specific date/time object provided as an argument.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ('time',)``


Module contents
===============

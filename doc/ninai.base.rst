
ninai.base package
******************


Submodules
==========


ninai.base.action module
========================

**class ninai.base.action.Action(*args)**

Bases: `ninai.base.constructor.Constructor <#ninai.base.constructor.Constructor>`_

``_abc_impl = <_abc._abc_data object>``

  **collect_thematic_relations(args)**

``core_arguments = ('predicate', 'thematic_relations')``

  **render(language: tfsl.languages.Language, context: Optional[list]
= None, framing: Optional[dict] = None)**

**class ninai.base.action.ActionRenderer(constructor, language,
context=None, framing=None)**

Bases: `ninai.base.constructor.ConstructorRenderer <#ninai.base.constructor.ConstructorRenderer>`_

  **get_predicate_clause()**

  **get_thematic_relation_spec(predicate_clause)**

  **process_relation(relation_type)**

  **render()**

Renders a constructor in language ``self.language``.


ninai.base.concept module
=========================

**class ninai.base.concept.Concept(*args)**

Bases: ``ninai.base.interfaces.Renderable``

Container for objects of types tfsl.{Item, Lexeme, LexemeSense}.

``_abc_impl = <_abc._abc_data object>``

  **render(language: tfsl.languages.Language, context: Optional[list]
= None, framing: Optional[dict] = None)**

Provides a Clause object representing ``object_in`` in language
``language_in``.


ninai.base.constants module
===========================


ninai.base.constructor module
=============================

**class ninai.base.constructor.Constructor(*args)**

Bases: ``ninai.base.interfaces.Renderable``

Base class for constructors.

``_abc_impl = <_abc._abc_data object>``

``core_arguments = ()``

  **classmethod register(language_in)**

Registers a renderer with a particular language.

  **classmethod register_all(method_in)**

Registers a renderer for use with *all* languages. Such
renderers primarily propagate information to other renderers and
do not output any Clause objects.

  **classmethod register_argument_post_hook(language_in, argument)**

  **classmethod register_argument_pre_hook(language_in, argument)**

  **render(language: tfsl.languages.Language, context: Optional[list]
= None, framing: Optional[dict] = None)**

  **classmethod set_argument_evaluation_order(language_in)**

Registers a core argument evaluation order for a given language.

**class ninai.base.constructor.ConstructorOutput(clause:
udiron.clause.Clause = None, scope: dict = <factory>)**

Bases: ``object``

``clause: udiron.clause.Clause = None``

``scope: dict``

**class ninai.base.constructor.ConstructorRenderer(constructor_in,
language_in, context_in=None, framing_settings=None)**

Bases: ``object``

  **get_argument_evaluation_order(constructor_in)**

  **process_argument(core_argument)**

  **render()**

Renders a constructor in language ``self.language``.

  **render_argument(core_argument, context, current_argument)**

  **render_inputs(renderer_scope)**

  **render_scope()**

  **run_post_hook(core_argument, context, current_outputs)**

  **run_pre_hook(core_argument, context, current_argument)**

**class ninai.base.constructor.ConstructorType(name, bases, namespace,
**kwargs)**

Bases: ``abc.ABCMeta``, ``type``

Metaclass for constructors.

This sets up the core argument list for each constructor based on
what’s provided in the ‘core_arguments’ parameter when declaring a
constructor (or what’s provided in the ‘core_arguments’ parameter
of its parent constructor and optionally what’s provided in the
‘extra_arguments’ parameter).

It also defines an argument container type for holding the inputs
to a renderer for that constructor (or, if the argument list is the
same as its parent, simply reuses the argument container type used
by that parent).

  **set_up_core_arguments(bases, **kwargs)**

  **set_up_inputs_class(bases, new_instance)**


ninai.base.graph module
=======================

**ninai.base.graph.build_relations(property_name)**

**ninai.base.graph.build_subgraphs(relations)**

**ninai.base.graph.build_translation_network(subgraphs,
property_list)**

**ninai.base.graph.build_translation_networks(subgraphs)**

**ninai.base.graph.find_demonym(concept_in, language_in,
fallback=None, **scope)**

**ninai.base.graph.find_or_fallback(concept_in, network_in,
language_in, fallback=None, **scope)**

**ninai.base.graph.find_predicate(concept_in, language_in,
fallback=None, **scope)**

**ninai.base.graph.find_sense(concept_in, language_in, fallback=None,
**scope)**

**ninai.base.graph.get_filename(property_name)**

**ninai.base.graph.get_shortest_paths(network, concept_in,
language_in)**

**ninai.base.graph.get_translation_networks()**

**ninai.base.graph.obtain_relations(property_name)**

**ninai.base.graph.process_candidates(candidate_paths, language_in,
**scope)**

**ninai.base.graph.read_config()**

**ninai.base.graph.read_relations_from_file(filename)**

**ninai.base.graph.register_sense_refine(language_in)**

**ninai.base.graph.search_graph(concept_in, network_in, language_in,
**scope)**


ninai.base.thematicrelation module
==================================

**class ninai.base.thematicrelation.Addressee(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q19720921'``

**class ninai.base.thematicrelation.Agent(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q392648'``

**class ninai.base.thematicrelation.Experiencer(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q1242505'``

**class ninai.base.thematicrelation.Patient(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q170212'``

**class ninai.base.thematicrelation.Recipient(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q20820253'``

**class ninai.base.thematicrelation.Source(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q31464082'``

**class ninai.base.thematicrelation.ThematicRelation(arg)**

Bases: ``ninai.base.interfaces.Renderable``

``_abc_impl = <_abc._abc_data object>``

``item = 'Q59496158'``

  **render(language: tfsl.languages.Language, context: Optional[list]
= None, framing: Optional[dict] = None)**

**class ninai.base.thematicrelation.ThematicRelationType(name, bases,
namespace, **kwargs)**

Bases: ``abc.ABCMeta``, ``type``

**class ninai.base.thematicrelation.Topic(arg)**

Bases: `ninai.base.thematicrelation.ThematicRelation <#ninai.base.thematicrelation.ThematicRelation>`_

``_abc_impl = <_abc._abc_data object>``

``item = 'Q22338337'``


ninai.base.utility module
=========================

**class ninai.base.utility.IdentityDict(arg)**

Bases: ``object``

Internally used dummy dictionary which returns *arg* if any key in
the dictionary is accessed.

**ninai.base.utility.cat_outputs(target, source)**

**ninai.base.utility.cat_scope_dicts(*scope_dicts)**

**ninai.base.utility.clause_from_item(concept_in, language_in,
**scope)**

**ninai.base.utility.gendered(sense_in)**

**ninai.base.utility.get_id_to_find(entity)**

**ninai.base.utility.is_desired_gender(sense_in, desired_gender)**

**ninai.base.utility.local_top_level(context)**

**ninai.base.utility.originating_constructor(clause)**

**ninai.base.utility.originating_parent_argument(clause)**

**ninai.base.utility.originating_parent_type(clause)**


Module contents
===============

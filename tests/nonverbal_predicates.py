import unittest

from tfsl import Q

import ninai.constructors.modifiers as mod
import ninai.constructors.nonverbal_predicates as np

class TestNonverbalPredicates(unittest.TestCase):
    def setUp(self):
        pass

    # def test_jupiter_bn(self):
    #     location_mod = mod.Location(None, Concept(Q(544)))
    #     largest_mod = mod.Superlative(Concept(Q(24245823)), location_mod)
    #     planet_arg = np.Attribution(Concept(Q(634)), largest_mod)
    #     main_stmt = np.Identification(Concept(Q(319)), planet_arg)
    #     self.assertIsNotNone(main_stmt)

# Identification(      # Jupiter is the largest planet in the solar system.
#   Q(319),
#   Attribution(       # The planet is the largest in the solar system.
#     Q(634),
#     Superlative(     # the largest in the solar system
#       Q(24245823),
#       scope=[Located( # in the solar system
#         Q(544))])))

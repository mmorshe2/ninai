# Ninai

* கண்தெரிந்து வழி நடக்கும்படி நினை. (Take care that you walk with your eyes open.)

## About

Ninai (from the classical Tamil for 'to think') is a statement construction system that uses, among other things, Wikidata items and lexemes with other grammatical primitives for the construction of complex assertions that can be rendered using [Udiron](https://bitbucket.org/mmorshe2/udiron/) into a given language.

(The author of this project is willing to relinquish the name Ninai to the set of constructors that will actually be in use on the Abstract Wikipedia, whether or not derived from what is developed here.)

## Setup

In addition to Udiron and its requirements, the networkx and SPARQLWrapper libraries are needed to query Wikidata and generate a network of translation/synonym relationships. These relationships are traversed to find an appropriate lexeme sense given an item as input.

Once you've done this, rename 'config.ini.example' to 'config.ini' and specify 1) where the
translation network should be stored ('CachePath') and how long (in seconds) this network should
be stored before regeneration ('TimeToLive').

## Use

There are two primary units of composition: the Constructor and the Concept.

* A Concept is a container for a Wikidata item, a Wikidata lexeme, or a sense on such a lexeme. When a Concept is rendered, a path from the contained item or sense (or for a lexeme, the first sense therein) to senses in the target language is found, and the lexeme containing that sense is used in the resulting syntax tree. (This process currently fails if the local translation network lacks a connection to an appropriate sense in the target language from the entity contained in the Concept.)
* A Constructor is a container for arguments to that constructor. On its own, it does nothing; when rendered, however, a language-specific constructor-specific function (or 'renderer') processes those arguments and returns some output, which can be either an output syntax tree and some contextual information or just the contextual information without the tree.

Some examples of Ninai's use may be found [here](demonstrations.md).

## Licensing

All code herein is Apache 2.0 unless otherwise stated below.

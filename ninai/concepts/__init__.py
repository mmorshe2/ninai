from tfsl import Language

from ninai import Concept

class GeneralDemonym(Concept):
    def render(self, language: Language, context: list = None, framing: dict = None):
        if framing is None:
            framing = {}
        return super().render(language, context, {'type': 'demonym'} | framing)

from ninai import Constructor

class StativeLocation(Constructor, core_arguments=('object',)):
    """Base class for the location of a constructor not in motion.
       These may be used, for example, as the second argument of Attribution."""

class ActionLocation(Constructor, core_arguments=('object',)):
    """Base class for the location of a constructor in motion.
       These may be used, for example, as scope arguments of other constructors."""

class Locative(StativeLocation):
    """
        Used to indicate the generic location of a particular object (akin to 'in', 'at', 'on')::
    
        >>> Attribution(Concept(Q(319)), Location(Concept(Q(544)))) # Jupiter is in the solar system.

        (See also general notes regarding StativeLocation.)
    """

class Adessive(StativeLocation):
    """Used to indicate that the containing constructor is near a particular object::

        >>> Attribution(Concept(Q(319)), Adessive(Concept(Q(544)))) # Jupiter is near the solar system.

        (See also general notes regarding StativeLocation.)
    """

class Subessive(StativeLocation):
    """Used to indicate that the containing constructor is below a particular object::

        >>> Attribution(Concept(Q(319)), Subessive(Concept(Q(544)))) # Jupiter is below the solar system.

        (See also general notes regarding StativeLocation.)
    """

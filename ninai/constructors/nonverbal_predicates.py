from ninai import Constructor

class Identification(Constructor, core_arguments=('subject', 'object')):
    """Equates subject and object::

       >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

       At the moment considered only as a top-level constructor.
    """

class Attribution(Constructor, core_arguments=('subject', 'attribute')):
    """Assigns an attribute to an object::

       >>> Attribution(Concept(Q(319)), Concept(Q(59863338))

       Independently the above may represent "Jupiter is large", a complete statement.
       As an argument elsewhere it may represent "large Jupiter", a modified concept.
    """

class Possession(Constructor, core_arguments=('possessor', 'possessed')):
    """Indicates that something owns something else::

       >>> Possession(Concept(Q(319)), Instance(Concept(Q(179792))))

       Independently it may represent "Jupiter has a ring", a complete statement.
       As an argument elsewhere it may represent "Jupiter's ring", a modified concept.
    """

class Benefaction(Constructor, core_arguments=('subject', 'beneficiary')):
    """Indicates that something is for the benefit of something else::

       >>> Benefaction(Concept(Q(319)), Instance(Concept(Q(58968)), Plural()))

       Independently it may represent "Jupiter is for intellectuals", a complete statement.
       As an argument elsewhere it may represent "Jupiter for intellectuals", a modified concept.
    """

class Existence(Constructor, core_arguments=('existent',)):
    """Indicates that something exists::

       >>> Existence(Instance(Concept(Q(634)))) # A planet exists; there is a planet.

       At the moment considered only as a top-level constructor.
    """

from ninai import Constructor

class Speaker(Constructor):
    """Refers to the speaker:

       >>> Attribution(Speaker(), Concept(Q(59863338))) # I am large.

       (This will likely be adjusted when a renderer for it is created.)
    """

class Listener(Constructor):
    """Refers to a listener::
    
       >>> Attribution(Listener(), Concept(Q(59863338))) # You are large.

       (This will likely be adjusted when a renderer for it is created.)
    """

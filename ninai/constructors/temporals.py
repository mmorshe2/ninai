from ninai import Constructor

class SpecificTemporal(Constructor, core_arguments=('time',)):
    """Base class for modifiers specifying the times of constructors based on a specific
       date/time object provided as an argument."""

class GeneralTemporal(Constructor):
    """Base class for modifiers specifying the generic times of constructors
       (akin to providing a tense)."""

class PastTemporal(GeneralTemporal):
    """Used for events occurring at any time before the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(89))), PastTemporal()) # I had an apple.

       (See also general notes regarding GeneralTemporal.)
    """

class Hesternal(PastTemporal):
    """Used for events occurring on the day prior to the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(89))), Hesternal()) # I had an apple yesterday.

       (See also general notes regarding GeneralTemporal.)
    """

class PresentTemporal(GeneralTemporal):
    """Used for events occurring at or around the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(165447))), PresentTemporal()) # I have a pen.

       (See also general notes regarding GeneralTemporal.)
    """

class Hodiernal(PresentTemporal):
    """Used for events occurring on the same day as the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(165447))), Hodiernal()) # I have a pen today.

       (See also general notes regarding GeneralTemporal.)
    """

class FutureTemporal(GeneralTemporal):
    """Used for events occurring at any time after the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(10817602))), FutureTemporal()) # I will have a pineapple.

       (See also general notes regarding GeneralTemporal.)
    """

class Crastinal(FutureTemporal):
    """Used for events occurring on the day following the current reference time::

       >>> Possession(Speaker(), Instance(Concept(Q(10817602))), Crastinal()) # I will have a pineapple tomorrow.

       (See also general notes regarding GeneralTemporal.)
    """

from ninai.constructors.locatives import *
from ninai.constructors.modifiers import *
from ninai.constructors.nominals import *
from ninai.constructors.nonverbal_predicates import *
from ninai.constructors.temporals import *

from tfsl import Language

import ninai.base.graph as graph
import ninai.base.constants as C
from ninai.base.constructor import Constructor, ConstructorRenderer
from ninai.base.thematicrelation import ThematicRelation
from ninai.base.utility import get_id_to_find

class Action(Constructor, core_arguments=('predicate','thematic_relations')):
    def __init__(self, *args):
        rearranged_args = self.collect_thematic_relations(args)
        super().__init__(*rearranged_args)

    def collect_thematic_relations(self, args):
        predicate = args[0]
        thematic_relations = {}
        new_args = []
        for argument in args[1:]:
            if isinstance(argument, ThematicRelation):
                thematic_relations[argument.item] = argument
            else:
                new_args.append(argument)
        return [predicate, thematic_relations] + new_args

    def render(self, language: Language, context: list = None, framing: dict = None):
        return ActionRenderer(self, language, context, framing).render()

class ActionRenderer(ConstructorRenderer):
    def __init__(self, constructor, language, context=None, framing=None):
        super().__init__(constructor, language, context, framing)
        self.thematic_relations = constructor.thematic_relations
        self.thematic_arguments = {}
        self.renderer_arguments = {}

    def get_thematic_relation_spec(self, predicate_clause):
        thematic_relation_stmts = predicate_clause.lexeme[predicate_clause.sense][C.has_thematic_relation]
        thematic_relation_spec = {}
        for stmt in thematic_relation_stmts:
            thematic_role_item = stmt[C.has_thematic_relation]
            if thematic_role_item is None:
                thematic_relation_spec['None'] = stmt
            else:
                thematic_role = thematic_role_item.id
                thematic_relation_spec[thematic_role] = stmt
        return thematic_relation_spec

    def process_relation(self, relation_type):
        relation = self.thematic_relations[relation_type]
        new_context = self.context + [(type(self.constructor), relation_type)]

        self.framing = self.run_pre_hook(relation_type, new_context, relation)

        current_outputs = self.render_argument(relation_type, new_context, relation)
        self.thematic_arguments[relation_type] = current_outputs

        self.framing = self.run_post_hook(relation_type, new_context, current_outputs)

    def get_predicate_clause(self):
        predicate = getattr(self.constructor, 'predicate')

        id_to_find = get_id_to_find(predicate)

        predicate_clause = graph.find_predicate(id_to_find, self.language, **self.framing)
        self.framing['thematic_relations'] = self.get_thematic_relation_spec(predicate_clause)

        return predicate_clause

    def render(self):
        """
        Renders a constructor in language ``self.language``.
        """
        predicate_clause = self.get_predicate_clause()

        renderer_scope = self.render_scope()
        self.renderer_arguments = {'predicate': predicate_clause}
        for relation_type in self.thematic_relations:
            self.process_relation(relation_type)

        self.renderer_arguments['thematic_relations'] = self.thematic_arguments
        return self.render_inputs(renderer_scope)

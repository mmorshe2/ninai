from abc import ABCMeta
from tfsl import Language

import ninai.base.constants as C
from ninai.base.interfaces import Renderable

class ThematicRelationType(ABCMeta,type):
    @classmethod
    def __prepare__(cls, name, bases, **kwargs):
        current_mapping = super().__prepare__(name, bases, **kwargs)
        return current_mapping

    def __new__(cls, name, bases, namespace, **kwargs):
        new_instance = super().__new__(cls, name, bases, namespace)

        new_instance.item = kwargs.get('item', C.not_yet_determined)

        return new_instance

    def __init__(cls, name, bases, namespace, **kwargs):
        super().__init__(name, bases, namespace)

class ThematicRelation(Renderable,metaclass=ThematicRelationType):
    def __init__(self, arg):
        self.arg = arg

    def render(self, language: Language, context: list = None, framing: dict = None):
        new_context = context + [(type(self), 'argument')]
        return self.arg.render(language, new_context, framing)

class Agent(ThematicRelation,item=C.agent):
    pass

class Patient(ThematicRelation,item=C.patient):
    pass

class Experiencer(ThematicRelation,item=C.experiencer):
    pass

class Addressee(ThematicRelation,item=C.addressee):
    pass

class Recipient(ThematicRelation,item=C.recipient):
    pass

class Topic(ThematicRelation,item=C.topic):
    pass

class Source(ThematicRelation,item=C.source):
    pass

import configparser
import os
import random
import time
from collections import defaultdict
from pathlib import Path

import networkx as nx
import SPARQLWrapper
import tfsl.utils
from tfsl import Q, L
from udiron import Clause, NonLexicalUnit

import ninai.base.constants as C

PREFIX_WD = 'http://www.wikidata.org/entity/'
PREFIX_P = 'http://www.wikidata.org/prop/direct/'
WDQS_ENDPOINT = 'https://query.wikidata.org/sparql'
query_str_sense_to_item = """
select ?i ?ilang ?j (wd:Q55872899 as ?jlang) {{
    ?i ^ontolex:sense/dct:language ?ilang.
    {{ ?i wdt:{} ?j }}
}}
"""
query_str_sense_to_sense = """
select ?i ?ilang ?j ?jlang {{
    ?i ^ontolex:sense/dct:language ?ilang.
    {{ ?i wdt:{} ?j .
       ?j ^ontolex:sense/dct:language ?jlang }}
}}
"""

# TODO: migrate this out when config is needed for more than just graph stuff
def read_config():
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / '../../config.ini').resolve()
    config.read(current_config_path)
    cpath = config['Ninai']['CachePath']
    ttl = float(config['Ninai']['TimeToLive'])
    return cpath, ttl

cache_path, time_to_live = read_config()
os.makedirs(cache_path,exist_ok=True)

def get_filename(property_name):
    return os.path.join(cache_path, f"subgraph_{property_name}.txt")

def build_relations(property_name):
    query_str = query_str_sense_to_item if property_name in [C.item_for_this_sense, C.predicate_for, C.demonym_of] else query_str_sense_to_sense
    sparql = SPARQLWrapper.SPARQLWrapper(WDQS_ENDPOINT, agent='ninai 0.0.1')
    sparql.setQuery(query_str.format(property_name))
    sparql.setReturnFormat(SPARQLWrapper.JSON)
    relations_out = sparql.query().convert()
    relations = []
    with open(get_filename(property_name), 'w') as relations_file:
        relations_file.write('\t'.join(relations_out['head']['vars']) + '\n')
        for relation in relations_out['results']['bindings']:
            new_relation = []
            for column_name in ['i', 'ilang', 'j', 'jlang']:
                new_relation.append(relation[column_name]['value'])
            relations.append(new_relation)
            relations_file.write('\t'.join(new_relation) + '\n')
    return relations

def read_relations_from_file(filename):
    assert time.time() - os.path.getmtime(filename) < time_to_live
    with open(filename) as fileptr:
        next(fileptr)
        relations = []
        for line in fileptr:
            components = line.rstrip('\n').split('\t')
            relations.append(components)
    return relations

def obtain_relations(property_name):
    try:
        filename = get_filename(property_name)
        relations = read_relations_from_file(filename)
    except (FileNotFoundError, OSError, AssertionError):
        relations = build_relations(property_name)
    return relations

target_property_list = [C.translation, C.synonym, C.antonym, C.troponym_of, C.hyperonym, C.pertainym, C.item_for_this_sense, C.predicate_for, C.demonym_of]

def get_translation_networks():
    relations = {}
    for property_name in target_property_list:
        relations[property_name] = obtain_relations(property_name)
    subgraphs = build_subgraphs(relations)
    return build_translation_networks(subgraphs)

def build_subgraphs(relations):
    subgraphs = {}
    for prop, relation_list in relations.items():
        G = nx.DiGraph()
        prop = prop.replace(PREFIX_P, '')
        for relation in relation_list:
            subj, subjlang, obj, objlang = [thing.replace(PREFIX_WD, '') for thing in relation]
            G.add_node(subj, lang=subjlang)
            G.add_node(obj, lang=objlang)
            G.add_edge(subj, obj, pred=prop)
            if objlang == C.wikidatan:
                G.add_edge(obj, subj, pred=prop)
        subgraphs[prop] = G
    return subgraphs

def build_translation_network(subgraphs, property_list):
    return nx.compose_all([subgraphs[prop] for prop in property_list])

def build_translation_networks(subgraphs):
    predicate_network = build_translation_network(subgraphs, [C.predicate_for, C.translation, C.synonym])
    substantive_network = build_translation_network(subgraphs, [C.item_for_this_sense, C.translation, C.synonym])
    demonym_network = build_translation_network(subgraphs, [C.demonym_of, C.translation, C.synonym])
    return predicate_network, substantive_network, demonym_network

PREDICATE_NETWORK, SUBSTANTIVE_NETWORK, DEMONYM_NETWORK = get_translation_networks()

__refine_candidate_paths__ = {}

def register_sense_refine(language_in):
    def stuff(method_in):
        __refine_candidate_paths__[language_in] = method_in
        return method_in
    return stuff

def process_candidates(candidate_paths, language_in, **scope):
    new_config = {}

    if len(candidate_paths) == 0:
        raise KeyError('How did you even get to this point?')
    elif len(candidate_paths) != 1:
        if refine_function := __refine_candidate_paths__.get(language_in, False):
            candidate_paths = refine_function(candidate_paths, new_config, **scope)
    sense_id = random.choice(list(candidate_paths.keys()))
    return Clause(L(sense_id), sense=sense_id, config=new_config)

def get_shortest_paths(network, concept_in, language_in):
    candidate_paths = {}
    shortest_paths = nx.shortest_path(network, concept_in)
    for node in shortest_paths:
        if network.nodes[node]['lang'] == language_in:
            pathgraph = nx.path_graph(shortest_paths[node])
            candidate_paths[node] = [network.edges[s, t] for (s, t) in pathgraph.edges()]
    return candidate_paths

def search_graph(concept_in, network_in, language_in, **scope):
    try:
        candidate_paths = get_shortest_paths(network_in, concept_in, language_in)
    except nx.exception.NodeNotFound:
        if tfsl.utils.matches_sense(concept_in):
            candidate_paths = {concept_in: []}
        else:
            candidate_paths = {}
    return candidate_paths

def find_or_fallback(concept_in, network_in, language_in, fallback=None, **scope):
    candidate_paths = search_graph(concept_in, network_in, language_in, **scope)
    if len(candidate_paths) == 0:
        if fallback:
            return fallback(concept_in, language_in, **scope)
        else:
            raise KeyError("Could not create clause for", concept_in, "in", language_in)
    return process_candidates(candidate_paths, language_in, **scope)

def find_sense(concept_in, language_in, fallback=None, **scope):
    return find_or_fallback(concept_in, SUBSTANTIVE_NETWORK, language_in, fallback, **scope)

def find_demonym(concept_in, language_in, fallback=None, **scope):
    return find_or_fallback(concept_in, DEMONYM_NETWORK, language_in, fallback, **scope)

def find_predicate(concept_in, language_in, fallback=None, **scope):
    return find_or_fallback(concept_in, PREDICATE_NETWORK, language_in, fallback, **scope)

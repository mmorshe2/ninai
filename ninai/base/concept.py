from tfsl import Language

import ninai.base.graph as graph
from ninai.base.interfaces import Renderable
from ninai.base.constructor import ConstructorOutput
from ninai.base.utility import get_id_to_find, clause_from_item

class Concept(Renderable):
    """
    Container for objects of types tfsl.{Item, Lexeme, LexemeSense}.
    """
    def __init__(self, *args):
        self.object = args[0]
        self.scope = args[1:]

    def render(self, language: Language, context: list = None, framing: dict = None):
        """
        Provides a Clause object representing ``object_in`` in language ``language_in``.
        """
        out = None
        if framing is None:
            framing = {}

        id_to_find = get_id_to_find(self.object)

        if framing and framing.get('type', False) == 'demonym':
            out = graph.find_demonym(id_to_find, language, **framing)
        else:
            out = graph.find_sense(id_to_find, language, fallback=clause_from_item, **framing)
        return ConstructorOutput(out, {})

# pylint: disable=invalid-name,wildcard-import,unused-wildcard-import

from udiron.constants import *

wikidatan = 'Q55872899'
not_yet_determined = 'Q59496158'

masculine = 'Q499327'
feminine = 'Q1775415'

male = 'Q6581097'
cis_male = 'Q15145778'
trans_male = 'Q2449503'
female = 'Q6581072'
cis_female = 'Q15145779'
trans_female = 'Q1052281'

sex_or_gender = 'P21'
object_has_role = 'P3831'
nature_of_statement = 'P5102'
item_for_this_sense = 'P5137'
grammatical_gender = 'P5185'
requires_grammatical_feature = 'P5713'
translation = 'P5972'
synonym = 'P5973'
antonym = 'P5974'
troponym_of = 'P5975'
demonym_of = 'P6271'
hyperonym = 'P6593'
pertainym = 'P8471'
predicate_for = 'P9970'
has_thematic_relation = 'P9971'

agent = 'Q392648'
patient = 'Q170212'
experiencer = 'Q1242505'
focus = 'Q1435289'
addressee = 'Q19720921'
recipient = 'Q20820253'
topic = 'Q22338337'
source = 'Q31464082'

from abc import ABCMeta, abstractmethod

from tfsl import Language

class Renderable(metaclass=ABCMeta):
    @abstractmethod
    def render(self, language: Language, context: list, framing: dict):
        pass
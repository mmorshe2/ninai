from tfsl.languages import Language

from ninai.base.interfaces import Renderable

class Sequential(Renderable):
    def __init__(self, *args):
        self.scope = args

    def render(self, language: Language, context: list = None, framing: dict = None):
        sentences = []
        for element in self.scope:
            sentence = element.render(language, context, framing)
            sentences.append(sentence)
        return sentences

    def __call__(self, language, context: list = None, framing: dict = None):
        return " ".join([str(clause) for clause in self.render(language, context, framing)])

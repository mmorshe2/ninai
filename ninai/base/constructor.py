from abc import ABCMeta
from collections import defaultdict
from dataclasses import dataclass, make_dataclass, field

from tfsl import Language
from udiron import Clause

from ninai.base.interfaces import Renderable
from ninai.base.utility import IdentityDict

default_inputs_entries = (('context', list), ('scope', dict), ('framing', dict))

@dataclass
class ConstructorOutput:
    clause: Clause = None
    scope: dict = field(default_factory=dict)

class ConstructorType(ABCMeta,type):
    """
    Metaclass for constructors.

    This sets up the core argument list for each constructor based
    on what's provided in the 'core_arguments' parameter when declaring
    a constructor (or what's provided in the 'core_arguments' parameter
    of its parent constructor and optionally what's provided in the
    'extra_arguments' parameter).

    It also defines an argument container type for holding the inputs to a renderer
    for that constructor (or, if the argument list is the same as its parent,
    simply reuses the argument container type used by that parent).
    """
    @classmethod
    def __prepare__(cls, name, bases, **kwargs):
        current_mapping = super().__prepare__(name, bases, **kwargs)
        return current_mapping

    def set_up_inputs_class(cls, bases, new_instance):
        if(len(bases) == 1 and
           hasattr(bases[0], 'core_arguments') and
           new_instance.core_arguments == bases[0].core_arguments):
            inputs_class = bases[0].Inputs
        else:
            inputs_entries = list(default_inputs_entries)
            for core_argument in new_instance.core_arguments:
                inputs_entries.append([core_argument, ConstructorOutput])
            inputs_class = make_dataclass('Inputs', tuple(inputs_entries))
        return inputs_class

    def set_up_core_arguments(cls, bases, **kwargs):
        parent_core_arguments = tuple()
        for base in bases:
            try:
                parent_core_arguments += base.core_arguments
            except AttributeError:
                pass

        current_core_arguments = kwargs.get('core_arguments', parent_core_arguments)
        extra_core_arguments = kwargs.get('extra_arguments', tuple())
        core_arguments = current_core_arguments + extra_core_arguments
        return core_arguments

    def __new__(cls, name, bases, namespace, **kwargs):
        new_instance = super().__new__(cls, name, bases, namespace)

        new_instance.core_arguments = cls.set_up_core_arguments(cls, bases, **kwargs)
        new_instance.__renderers__ = {}
        new_instance.__argument_eval_orders__ = {}
        new_instance.__pre_hooks__ = defaultdict(dict)
        new_instance.__post_hooks__ = defaultdict(dict)
        new_instance.Inputs = cls.set_up_inputs_class(cls, bases, new_instance)

        return new_instance

    def __init__(cls, name, bases, namespace, **kwargs):
        super().__init__(name, bases, namespace)

class Constructor(Renderable,metaclass=ConstructorType):
    """
    Base class for constructors.
    """

    # the following are defined specifically to make linting work
    core_arguments = tuple()
    __renderers__ = {}
    __argument_eval_orders__ = {}
    __pre_hooks__ = defaultdict(dict)
    __post_hooks__ = defaultdict(dict)
    Inputs = make_dataclass('Inputs', default_inputs_entries)

    def __init__(self, *args):
        super().__init__()
        index = 0
        for index, core_argument in enumerate(self.core_arguments):
            setattr(self, core_argument, args[index])
        if index == 0:
            self.scope = args
        else:
            self.scope = args[index+1:]

    @classmethod
    def register(cls, language_in):
        """
        Registers a renderer with a particular language.
        """
        def register_method(method_in):
            cls.__renderers__[language_in] = method_in
            return method_in
        return register_method

    @classmethod
    def register_all(cls, method_in):
        """
        Registers a renderer for use with *all* languages.
        Such renderers primarily propagate information to other renderers
        and do not output any Clause objects.
        """
        cls.__renderers__ = IdentityDict(method_in)
        return method_in

    @classmethod
    def set_argument_evaluation_order(cls, language_in):
        """Registers a core argument evaluation order for a given language."""
        def set_arg_eval_order(order):
            if len(order) != len(cls.core_arguments):
                raise ValueError("Order of arguments for", cls.__name__, "not equal to number of arguments")
            cls.__argument_eval_orders__[language_in] = order
        return set_arg_eval_order

    @classmethod
    def register_argument_pre_hook(cls, language_in, argument):
        def set_arg_pre_hook(method_in):
            cls.__pre_hooks__[language_in][argument] = method_in
        return set_arg_pre_hook

    @classmethod
    def register_argument_post_hook(cls, language_in, argument):
        def set_arg_post_hook(method_in):
            cls.__post_hooks__[language_in][argument] = method_in
        return set_arg_post_hook

    def __call__(self, language, context: list = None, framing: dict = None):
        return str(self.render(language, context, framing))

    def render(self, language: Language, context: list = None, framing: dict = None):
        return ConstructorRenderer(self, language, context, framing).render()

class ConstructorRenderer:
    def __init__(self, constructor_in, language_in, context_in=None, framing_settings=None):
        self.constructor = constructor_in
        self.inputs = constructor_in.Inputs
        self.pre_hooks = constructor_in.__pre_hooks__
        self.post_hooks = constructor_in.__post_hooks__
        self.renderers = constructor_in.__renderers__
        self.scope = constructor_in.scope
        self.language = language_in
        self.context = [] if context_in is None else context_in
        self.framing = {} if framing_settings is None else framing_settings
        self.argument_evaluation_order = self.get_argument_evaluation_order(constructor_in)
        self.renderer_arguments = {}

    def get_argument_evaluation_order(self, constructor_in):
        try:
            argument_evaluation_order = constructor_in.__argument_eval_orders__[self.language]
        except KeyError:
            argument_evaluation_order = constructor_in.core_arguments
        return argument_evaluation_order

    def run_pre_hook(self, core_argument, context, current_argument):
        try:
            pre_hook = self.pre_hooks[self.language][core_argument]
            return pre_hook(current_argument, context, self.framing)
        except KeyError:
            return self.framing

    def run_post_hook(self, core_argument, context, current_outputs):
        try:
            post_hook = self.post_hooks[self.language][core_argument]
            return post_hook(current_outputs, context, self.framing)
        except KeyError:
            return self.framing

    def render_inputs(self, renderer_scope):
        renderer_inputs = self.inputs(self.context, renderer_scope, self.framing, **self.renderer_arguments)
        renderer_output = self.renderers[self.language](renderer_inputs)
        if len(self.context) == 0:
            return renderer_output.clause
        return renderer_output

    def render_argument(self, core_argument, context, current_argument):
        current_outputs = current_argument.render(self.language, context, self.framing)
        current_outputs.clause.config['_tracking'] = (type(self), core_argument, type(current_argument))
        return current_outputs

    def render_scope(self):
        renderer_scope = {'outputs': defaultdict(list)}
        new_context_scope = self.context + [(type(self.constructor), 'scope')]
        for scope_object in self.scope:
            scope_output = scope_object.render(self.language, new_context_scope)
            if scope_output.clause is not None:
                renderer_scope['outputs'][type(scope_object)].append(scope_output)
            renderer_scope.update(scope_output.scope)
        return renderer_scope

    def process_argument(self, core_argument):
        current_argument = getattr(self.constructor, core_argument)
        new_context = self.context + [(type(self.constructor), core_argument)]

        self.framing = self.run_pre_hook(core_argument, new_context, current_argument)

        current_outputs = self.render_argument(core_argument, new_context, current_argument)
        self.renderer_arguments[core_argument] = current_outputs

        self.framing = self.run_post_hook(core_argument, new_context, current_outputs)

    def render(self):
        """
        Renders a constructor in language ``self.language``.
        """
        renderer_scope = self.render_scope()
        for core_argument in self.argument_evaluation_order:
            self.process_argument(core_argument)

        return self.render_inputs(renderer_scope)

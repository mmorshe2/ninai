import tfsl.utils
from tfsl import Item, Lexeme, LexemeSense, L, Q
from udiron import Clause, NonLexicalUnit

import ninai.base.constants as C

class IdentityDict:
    """
    Internally used dummy dictionary which returns `arg` if any key in the dictionary is accessed.
    """
    def __init__(self, arg):
        self.arg = arg

    def __getitem__(self, key):
        return self.arg

def get_id_to_find(entity):
    if isinstance(entity, (Item, LexemeSense)):
        id_to_find = entity.id
    elif isinstance(entity, Lexeme):
        id_to_find = entity.get_senses()[0]
    return id_to_find

def is_desired_gender(sense_in, desired_gender):
    lex = L(sense_in)
    gender_stmts = lex[C.grammatical_gender]
    if len(gender_stmts) != 0:
        gender_value = gender_stmts[0][C.grammatical_gender]
        return gender_value == desired_gender
    return False

def gendered(sense_in):
    lex = L(sense_in)
    return len(lex[C.grammatical_gender]) != 0

def clause_from_item(concept_in, language_in, **scope):
    if tfsl.utils.matches_item(concept_in):
        item = Q(concept_in)
        target_label = item.labels[language_in.code] @ language_in
        return Clause(NonLexicalUnit(target_label, lang=language_in))
    raise KeyError("Could not convert", concept_in, "into clause")

def cat_outputs(target, source):
    for k, v in source.items():
        if k in target.keys():
            target[k] += v
        else:
            target[k] = v
    return target

def cat_scope_dicts(*scope_dicts):
    output_dict = {}
    for scope_dict in scope_dicts:
        for k, v in scope_dict.items():
            if k in output_dict.keys():
                if k == 'outputs':
                    output_dict['outputs'] = cat_outputs(output_dict['outputs'], scope_dict['outputs'])
                else:
                    output_dict[k] = v
            else:
                output_dict[k] = v
    return output_dict

local_top_level = lambda context: not context

originating_parent_type = lambda clause: clause.config['_tracking'][0]
originating_parent_argument = lambda clause: clause.config['_tracking'][1]
originating_constructor = lambda clause: clause.config['_tracking'][2]

# TODO: for names, have a utility function to assemble from name lexemes?

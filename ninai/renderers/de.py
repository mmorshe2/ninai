from tfsl import langs
import udiron.langs.de as de
import udiron.langs.mul as mul

from ninai import ConstructorOutput, Concept
from ninai import Constants as C
from ninai.base.action import Action
from ninai.base.utility import gendered, is_desired_gender, local_top_level, originating_constructor, cat_scope_dicts
from ninai.base.graph import register_sense_refine
import ninai.constructors as Con

def process_scope_outputs(attribute_clause, outputs):
    for key in outputs:
        if issubclass(key, Con.StativeLocation):
            for entry in outputs[key]:
                attribute_clause = de.add_location_modifier(attribute_clause, entry.clause)
    return attribute_clause

@Con.Identification.register(langs.de_)
def render_identification_de(inputs: Con.Identification.Inputs) -> ConstructorOutput:
    object_clause = de.inflect_for_case(inputs.object.clause, C.nominative)
    object_clause = de.add_copular_subject(object_clause, inputs.subject.clause, **inputs.scope)
    object_clause = mul.full_stop(object_clause)

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.object.scope)
    return ConstructorOutput(object_clause, output_scope)

@Con.Identification.register_argument_pre_hook(langs.de_, 'subject')
def pre_hook_identification_subject_de(subject, context, framing):
    if isinstance(subject, Concept):
        gender_stmts = subject.object[C.sex_or_gender]
        if len(gender_stmts) != 0:
            gender_value = gender_stmts[0][C.sex_or_gender]
            if gender_value in [C.male, C.cis_male, C.trans_male]:
                framing['subject_gender'] = 'masc'
            elif gender_value in [C.female, C.cis_female, C.trans_female]:
                framing['subject_gender'] = 'fem'
    return framing

@Con.Attribution.register_argument_post_hook(langs.de_, 'subject')
def post_hook_attribution_subject_de(subject, context, framing):
    if subject.clause.config.get('subject_gender', False):
        framing['subject_gender'] = '_'
    return framing

@Con.Attribution.register(langs.de_)
def render_attribution_de(inputs: Con.Attribution.Inputs) -> ConstructorOutput:
    if local_top_level(inputs.context):
        attribute_clause = de.inflect_for_gender(inputs.attribute.clause, inputs.subject.clause)
        attribute_clause = de.add_copular_subject(attribute_clause, inputs.subject.clause)
        attribute_clause = mul.full_stop(attribute_clause)
    elif issubclass(originating_constructor(inputs.attribute.clause), Con.StativeLocation):
        attribute_clause = de.add_location_modifier(inputs.subject.clause, inputs.attribute.clause)
    else:
        attribute_clause = de.inflect_for_gender(inputs.attribute.clause, inputs.subject.clause)
        attribute_clause = de.add_adjectival_modifier(inputs.subject.clause, attribute_clause)
    attribute_clause = process_scope_outputs(attribute_clause, inputs.attribute.scope.get('outputs', {}))

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.attribute.scope)
    return ConstructorOutput(attribute_clause, output_scope)

@Con.Instance.register(langs.de_)
def render_instance_de(inputs: Con.Instance.Inputs) -> ConstructorOutput:
    subject_clause = de.add_noun_count(inputs.subject.clause, **inputs.scope)

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope)
    return ConstructorOutput(subject_clause, output_scope)

@Con.Locative.register(langs.de_)
def render_locative_de(inputs: Con.Locative.Inputs) -> ConstructorOutput:
    object_clause = de.mark_as_location(inputs.object.clause)
    return ConstructorOutput(object_clause)

@register_sense_refine(langs.de_)
def sense_refine_de(candidate_paths, new_config, **scope):
    if 'subject_gender' in scope:
        if scope['subject_gender'] == '_':
            candidate_paths = {k: v for (k, v) in candidate_paths.items() if not gendered(k)}
        else:
            desired_gender = C.feminine if scope['subject_gender'] == 'fem' else C.masculine
            candidate_paths = {k: v for (k, v) in candidate_paths.items() if is_desired_gender(k, desired_gender)}
            new_config['subject_gender'] = desired_gender
    return candidate_paths

@Action.register(langs.de_)
def render_action_de(inputs: Action.Inputs) -> ConstructorOutput:
    output_clause = de.pop_if_needed(inputs.predicate)
    for thematic_rel, stmt in inputs.framing['thematic_relations'].items():
        if thematic_rel == 'None':
            output_clause = de.add_empty_subject(output_clause)
            continue
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value.id
        # TODO: check for other qualifiers

        current_clause = inputs.thematic_relations[thematic_rel].clause
        current_clause.add_inflections(grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_clause = de.add_nominal_subject(output_clause, current_clause)
        elif syntactic_role == C.direct_object:
            output_clause = de.add_nominal_object(output_clause, current_clause)
        elif syntactic_role == C.indirect_object:
            output_clause = de.add_indirect_object(output_clause, current_clause)
    output_clause = de.inflect_for_tense(output_clause, **inputs.scope)
    if local_top_level(inputs.context):
        output_clause = mul.full_stop(output_clause)
    return ConstructorOutput(output_clause)

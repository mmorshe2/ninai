# import attr # consider rewriting classes using this
# import funcy, pydash, fnc # see if alternatives in str might be broken up (funcy.juxt)
# import more_itertools # make iterations more brief
# import phantom # adjust away uses of None
# import returns # adjust away uses of None
# from box import Box # pack arguments into a box

from tfsl import langs
import udiron.langs.bn as bn
import udiron.langs.mul as mul

from ninai import ConstructorOutput
from ninai.base.action import Action
from ninai.base.utility import local_top_level, cat_scope_dicts
import ninai.constructors as Con
import ninai.base.constants as C
import ninai.base.thematicrelation as TR

def process_scope_outputs(base_clause, outputs):
    # TODO: scrap certain outputs if added to clause?
    for key in outputs:
        if issubclass(key, Con.StativeLocation):
            for entry in outputs[key]:
                base_clause = bn.add_location_modifier(base_clause, entry.clause)
    return base_clause

@Con.Identification.register(langs.bn_)
def render_identification_bn(inputs: Con.Identification.Inputs) -> ConstructorOutput:
    object_clause = bn.add_copular_subject(inputs.object.clause, inputs.subject.clause)
    object_clause = mul.danda(object_clause)
    object_clause = process_scope_outputs(object_clause, inputs.scope.get('outputs', {}))

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.object.scope)
    return ConstructorOutput(object_clause, output_scope)

@Con.Instance.register(langs.bn_)
def render_instance_bn(inputs: Con.Instance.Inputs) -> ConstructorOutput:
    subject_clause = bn.add_noun_count(inputs.subject.clause, **inputs.scope)

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope)
    return ConstructorOutput(subject_clause, output_scope)

@Con.Attribution.register(langs.bn_)
def render_attribution_bn(inputs: Con.Attribution.Inputs) -> ConstructorOutput:
    if local_top_level(inputs.context):
        attribute_clause = bn.add_copular_subject(inputs.attribute.clause, inputs.subject.clause, **inputs.scope)
        attribute_clause = process_scope_outputs(attribute_clause, inputs.scope.get('outputs', {}))
        attribute_clause = mul.danda(attribute_clause)
    else:
        # TODO: handle other modifiers here or just add them to scope handling?
        attribute_clause = bn.add_adjectival_modifier(inputs.subject.clause, inputs.attribute.clause)
        attribute_clause = process_scope_outputs(attribute_clause, inputs.scope.get('outputs', {}))

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.attribute.scope)
    return ConstructorOutput(attribute_clause, output_scope)

@Con.Superlative.register(langs.bn_)
def render_superlative_bn(inputs: Con.Superlative.Inputs) -> ConstructorOutput:
    attribute_clause = bn.make_adjective_superlative(inputs.attribute.clause)
    attribute_clause = process_scope_outputs(attribute_clause, inputs.scope.get('outputs', {}))

    output_scope = cat_scope_dicts(inputs.scope, inputs.attribute.scope)
    return ConstructorOutput(attribute_clause, output_scope)

@Con.Locative.register(langs.bn_)
def render_locative_bn(inputs: Con.Locative.Inputs) -> ConstructorOutput:
    object_clause = bn.mark_locative_case(inputs.object.clause)
    return ConstructorOutput(object_clause)

@Con.Existence.register(langs.bn_)
def render_existence_bn(inputs: Con.Existence.Inputs) -> ConstructorOutput:
    acha_clause = bn.note_existence(inputs.existent.clause, **cat_scope_dicts(inputs.scope, inputs.existent.scope))
    acha_clause = process_scope_outputs(acha_clause, inputs.scope.get('outputs', {}))
    if local_top_level(inputs.context):
        acha_clause = mul.danda(acha_clause)
    return ConstructorOutput(acha_clause)

@Con.Negation.register(langs.bn_)
def render_negation_bn(inputs: Con.Negation.Inputs) -> ConstructorOutput:
    constructor_clause = bn.negate_clause(inputs.constructor.clause)
    if local_top_level(inputs.context):
        constructor_clause = mul.danda(constructor_clause)

    output_scope = cat_scope_dicts(inputs.scope, inputs.constructor.scope)
    return ConstructorOutput(constructor_clause, output_scope)

@Con.Proximal.register(langs.bn_)
def render_proximal_bn(inputs: Con.Proximal.Inputs) -> ConstructorOutput:
    emphasize = "emphasis" in inputs.scope
    output_clause = bn.attach_demonstrative_determiner(inputs.constructor.clause, 'proximal', emphasize=emphasize)
    return ConstructorOutput(output_clause)

@Con.MedialDemonstrative.register(langs.bn_)
def render_medial_bn(inputs: Con.MedialDemonstrative.Inputs) -> ConstructorOutput:
    emphasize = "emphasis" in inputs.scope
    output_clause = bn.attach_demonstrative_determiner(inputs.constructor.clause, 'medial', emphasize=emphasize)
    return ConstructorOutput(output_clause)

@Con.Distal.register(langs.bn_)
def render_distal_bn(inputs: Con.Distal.Inputs) -> ConstructorOutput:
    emphasize = "emphasis" in inputs.scope
    output_clause = bn.attach_demonstrative_determiner(inputs.constructor.clause, 'distal', emphasize=emphasize)
    return ConstructorOutput(output_clause)

@Action.register(langs.bn_)
def render_action_bn(inputs: Action.Inputs) -> ConstructorOutput:
    # TODO: can popping be delayed at all?
    # TODO: determine if popping needs to be done at all
    output_clause = inputs.predicate.pop()
    for thematic_rel, stmt in inputs.framing['thematic_relations'].items():
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value.id
        # TODO: check for other qualifiers

        current_clause = inputs.thematic_relations[thematic_rel].clause
        current_clause = bn.add_thematic_inflections(current_clause, grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_clause = bn.add_nominal_subject(output_clause, current_clause)
            output_clause = bn.inflect_for_person(output_clause, current_clause)
        elif syntactic_role == C.direct_object:
            output_clause = bn.add_nominal_object(output_clause, current_clause)
    output_clause = bn.inflect_for_tense(output_clause, **inputs.scope)
    if local_top_level(inputs.context):
        output_clause = mul.danda(output_clause)
    return ConstructorOutput(output_clause)

@Con.Speaker.register(langs.bn_)
def render_speaker_bn(inputs: Con.Speaker.Inputs) -> ConstructorOutput:
    output_clause = bn.generate_speaker(**inputs.scope)
    return ConstructorOutput(output_clause)

@Con.Listener.register(langs.bn_)
def render_listener_bn(inputs: Con.Listener.Inputs) -> ConstructorOutput:
    output_clause = bn.generate_listener(**inputs.scope)
    return ConstructorOutput(output_clause)

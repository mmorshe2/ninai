from collections import defaultdict

from tfsl import langs
import udiron.langs.sv as sv
import udiron.langs.mul as mul

from ninai import ConstructorOutput
from ninai.base.action import Action
from ninai.base.utility import local_top_level, cat_scope_dicts
import ninai.constructors as Con
import ninai.base.constants as C

def process_scope_outputs(attribute_clause, outputs):
    for key in outputs:
        if issubclass(key, Con.StativeLocation):
            for entry in outputs[key]:
                attribute_clause = sv.add_location_modifier(attribute_clause, entry.clause)
    return attribute_clause

def propagate_scope_outputs(outputs):
    scope_outputs = defaultdict(list)
    for key in outputs:
        if issubclass(key, Con.StativeLocation):
            for entry in outputs[key]:
                scope_outputs[key].append(entry)
    return {'outputs': scope_outputs}

@Con.Identification.register(langs.sv_)
def render_identification_sv(inputs: Con.Identification.Inputs) -> ConstructorOutput:
    object_clause = sv.add_copular_subject(inputs.object.clause, inputs.subject.clause)
    object_clause = mul.full_stop(object_clause)

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.object.scope)
    return ConstructorOutput(object_clause, output_scope)

@Con.Instance.register(langs.sv_)
def render_instance_sv(inputs: Con.Instance.Inputs) -> ConstructorOutput:
    subject_clause = sv.add_noun_count(inputs.subject.clause, **inputs.scope)

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope)
    return ConstructorOutput(subject_clause, output_scope)

@Con.Attribution.register(langs.sv_)
def render_attribution_sv(inputs: Con.Attribution.Inputs) -> ConstructorOutput:
    attribute_clause = sv.inflect_for_gender(inputs.attribute.clause, inputs.subject.clause)
    if local_top_level(inputs.context):
        attribute_clause = sv.add_copular_subject(attribute_clause, inputs.subject.clause)
        attribute_clause = mul.full_stop(attribute_clause)
    else:
        attribute_clause = sv.add_adjectival_modifier(inputs.subject.clause, attribute_clause)
    attribute_clause = process_scope_outputs(attribute_clause, inputs.attribute.scope.get('outputs', {}))

    output_scope = cat_scope_dicts(inputs.scope, inputs.subject.scope, inputs.attribute.scope)
    return ConstructorOutput(attribute_clause, output_scope)

@Con.Superlative.register(langs.sv_)
def render_superlative_sv(inputs: Con.Superlative.Inputs) -> ConstructorOutput:
    attribute_clause = sv.make_adjective_superlative(inputs.attribute.clause)

    output_scope = propagate_scope_outputs(inputs.scope.get('outputs', {}))
    return ConstructorOutput(attribute_clause, output_scope)

@Con.Locative.register(langs.sv_)
def render_locative_sv(inputs: Con.Locative.Inputs) -> ConstructorOutput:
    object_clause = sv.mark_as_location(inputs.object.clause)
    return ConstructorOutput(object_clause)

@Con.Existence.register(langs.sv_)
def render_existence_sv(inputs: Con.Existence.Inputs) -> ConstructorOutput:
    existence_clause = sv.build_existence_clause(inputs.existent.clause, **cat_scope_dicts(inputs.scope, inputs.existent.scope))
    if local_top_level(inputs.context):
        existence_clause = mul.full_stop(existence_clause)
    return ConstructorOutput(existence_clause)

@Con.Negation.register(langs.sv_)
def render_negation_sv(inputs: Con.Negation.Inputs) -> ConstructorOutput:
    negated_clause = sv.negate_clause(inputs.constructor.clause)
    if local_top_level(inputs.context):
        negated_clause = mul.full_stop(negated_clause)
    return ConstructorOutput(negated_clause)

@Action.register(langs.sv_)
def render_action_sv(inputs: Action.Inputs) -> ConstructorOutput:
    output_clause = inputs.predicate.pop()
    for thematic_rel, stmt in inputs.framing['thematic_relations'].items():
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value.id
        # TODO: check for other qualifiers

        current_clause = inputs.thematic_relations[thematic_rel].clause
        current_clause = sv.add_thematic_inflections(current_clause, grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_clause = sv.add_nominal_subject(output_clause, current_clause)
        elif syntactic_role == C.direct_object:
            output_clause = sv.add_nominal_object(output_clause, current_clause)
    output_clause = sv.inflect_for_tense(output_clause, **inputs.scope)
    if local_top_level(inputs.context):
        output_clause = mul.full_stop(output_clause)
    return ConstructorOutput(output_clause)

@Con.Speaker.register(langs.sv_)
def render_speaker_sv(inputs: Con.Speaker.Inputs) -> ConstructorOutput:
    output_clause = sv.generate_speaker(**inputs.scope)
    return ConstructorOutput(output_clause)

@Con.Listener.register(langs.sv_)
def render_listener_sv(inputs: Con.Listener.Inputs) -> ConstructorOutput:
    output_clause = sv.generate_listener(**inputs.scope)
    return ConstructorOutput(output_clause)

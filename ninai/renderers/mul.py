from ninai import ConstructorOutput
import ninai.constructors as Con

@Con.Definite.register_all
def render_definite(inputs: Con.Definite.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'definite': True})

@Con.PastTemporal.register_all
def render_pasttemporal(inputs: Con.PastTemporal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'past'}})

@Con.PresentTemporal.register_all
def render_presenttemporal(inputs: Con.PresentTemporal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'present'}})

@Con.FutureTemporal.register_all
def render_futuretemporal(inputs: Con.FutureTemporal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'future'}})

@Con.Hesternal.register_all
def render_hesternal(inputs: Con.Hesternal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'past', 'refine': 'hesternal'}})

@Con.Hodiernal.register_all
def render_hodiernal(inputs: Con.Hodiernal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'present', 'refine': 'hodiernal'}})

@Con.Crastinal.register_all
def render_crastinal(inputs: Con.Crastinal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'tense': {'main': 'future', 'refine': 'crastinal'}})

@Con.Emphasis.register_all
def render_emphasis(inputs: Con.Emphasis.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'emphasis': ""})

@Con.Singular.register_all
def render_singular(inputs: Con.Singular.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'number': "singular"})

@Con.Plural.register_all
def render_plural(inputs: Con.Plural.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'number': "plural"})

@Con.Paucal.register_all
def render_paucal(inputs: Con.Paucal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'number': "paucal"})

@Con.Informal.register_all
def render_informal(inputs: Con.Informal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'formality': "informal"})

@Con.Familiar.register_all
def render_familiar(inputs: Con.Familiar.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'formality': "familiar"})

@Con.Formal.register_all
def render_formal(inputs: Con.Formal.Inputs) -> ConstructorOutput:
    return ConstructorOutput(scope={'formality': "formal"})

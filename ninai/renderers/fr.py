from collections import defaultdict

from tfsl import langs
import udiron.langs.fr as fr
import udiron.langs.mul as mul

from ninai import ConstructorOutput
from ninai.base.action import Action
from ninai.base.utility import local_top_level, cat_scope_dicts
import ninai.constructors as Con
import ninai.base.constants as C

@Action.register(langs.fr_)
def render_action_fr(inputs: Action.Inputs) -> ConstructorOutput:
    output_clause = inputs.predicate.pop()
    for thematic_rel, stmt in inputs.framing['thematic_relations'].items():
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value.id
        # TODO: check for other qualifiers

        current_clause = inputs.thematic_relations[thematic_rel].clause
        current_clause.add_inflections(grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_clause = fr.add_nominal_subject(output_clause, current_clause)
            output_clause = fr.inflect_for_person(output_clause, current_clause)
            output_clause = fr.inflect_for_number(output_clause, current_clause)
        elif syntactic_role == C.direct_object:
            output_clause = fr.add_nominal_object(output_clause, current_clause)
    output_clause = fr.inflect_for_tense(output_clause, **inputs.scope)
    if local_top_level(inputs.context):
        output_clause = mul.full_stop(output_clause)
    return ConstructorOutput(output_clause)

@Con.Speaker.register(langs.fr_)
def render_speaker_fr(inputs: Con.Speaker.Inputs) -> ConstructorOutput:
    output_clause = fr.generate_speaker(**inputs.scope)
    return ConstructorOutput(output_clause)

@Con.Listener.register(langs.fr_)
def render_listener_fr(inputs: Con.Listener.Inputs) -> ConstructorOutput:
    output_clause = fr.generate_listener(**inputs.scope)
    return ConstructorOutput(output_clause)

# Demonstrations of Ninai

This is a list of demonstration utterances.
Most work in Bengali, and many of the same work in Swedish;
only one of these concerns German.

## Jupiter

These build up to the sentence "Jupiter is the largest planet in the solar system.",
the first example sentence with regard to Abstract Wikipedia rendering,
in Bengali and Swedish.

```python
from tfsl import L, Q, langs

import ninai.renderers
from ninai import Concept
from ninai.constructors import *

for LANG in [langs.bn_, langs.sv_]:
    for sentence in [
        Identification(Concept(Q(319)), Concept(Q(634))),
        Identification(Concept(Q(319)), Instance(Concept(Q(634)))),
        Identification(Concept(Q(319)), Instance(Concept(Q(634)), Definite())),
        Attribution(Concept(Q(634)), Concept(Q(59863338))),
        Attribution(Instance(Concept(Q(634))), Concept(Q(59863338))),
        Attribution(Instance(Concept(Q(634)), Definite()), Concept(Q(59863338))),
        Identification(Concept(Q(319)), Instance(Attribution(Concept(Q(634)), Concept(Q(59863338))))),
        Identification(Concept(Q(319)), Instance(Attribution(Concept(Q(634)), Concept(Q(59863338))), Definite())),
        Identification(Concept(Q(319)), Instance(Attribution(Concept(Q(634)), Superlative(Concept(Q(59863338)))), Definite())),
        Identification(Concept(Q(319)), Instance(Attribution(Concept(Q(634)), Superlative(Concept(Q(59863338)), Locative(Concept(Q(544))))), Definite()))
    ]:
        print(sentence(LANG))
```

## Kishore Kumar (1)

This renders the opening lines of [this song](https://www.youtube.com/watch?v=KigjSXLBorc)
(and adds a variant thereof)
in Bengali and Swedish.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept
from ninai.constructors import *

for LANG in [langs.bn_,langs.sv_]:
    for sentence in [
        Existence(Concept(Q(190507)), PastTemporal()),
        Existence(Concept(Q(316)), PastTemporal()),
        Negation(Existence(Concept(Q(190507)), Hodiernal())),
        Negation(Existence(Concept(Q(316)), Hodiernal())),
        Existence(Concept(Q(190507)), Crastinal()),
        Existence(Concept(Q(316)), Crastinal())
    ]:
        print(sentence(LANG))
```

## Kishore Kumar (2)

This attempts to render the opening lines of [this song](https://www.youtube.com/watch?v=07BadGcIEe4)
in Bengali.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept
from ninai.constructors import *

for sentence in [
    Attribution(Concept(Q(575)), Concept(Q(3635662)), PastTemporal(), Locative(Distal(Concept(Q(575)), Emphasis()))),
    Existence(Concept(Q(1075)), Locative(Attribution(Concept(Q(7391292)), Concept(Q(2765162)))), PastTemporal())
]:
    print(sentence(langs.bn_))
```

## Descriptions of places and people

This was created by request of User:Nikki
for German.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept
from ninai.concepts import GeneralDemonym
from ninai.constructors import *

for sentence in [
    Identification(Concept(Q(4191)), Instance(Attribution(Concept(Q(515)), Locative(Concept(Q(39)))))),
    Identification(Concept(Q(980)), Instance(Attribution(Concept(Q(1221156)), Locative(Concept(Q(183)))))),
    Identification(Concept(Q(42)), Instance(Attribution(Concept(Q(36180)), GeneralDemonym(Q(145)))), PastTemporal()),
    Identification(Concept(Q(564328)), Instance(Attribution(Concept(Q(82955)), GeneralDemonym(Q(183))))),
]:
    print(sentence(langs.de_))
```

## Planet interactions

This produces some contrived but grammatical sentences concerning the planets Saturn and Jupiter
in Bengali and Swedish.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept, Action, Agent, Patient
from ninai.constructors import *

for LANG in [langs.bn_, langs.sv_]:
    for sentence in [
        Action(Q(844482), Agent(Concept(Q(193))), Patient(Concept(Q(319))), Hesternal()),
        Action(Q(844482), Agent(Concept(Q(193))), Hodiernal(), Patient(Concept(Q(319)))),
        Action(Q(844482), Crastinal(), Patient(Concept(Q(319))), Agent(Concept(Q(193)))),
        Action(Q(206021), Agent(Concept(Q(193))), Hesternal()),
        Action(Q(206021), Hodiernal(), Agent(Concept(Q(193)))),
        Action(Q(206021), Agent(Concept(Q(193))), Crastinal())
    ]:
        print(sentence(LANG))
```
